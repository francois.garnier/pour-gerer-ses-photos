---
title: "Pour gérer ses photos"
order: 0
in_menu: true
---
<h1>Quelques incontournables...</h1>

<h2>Gérer sa collection de photos</h2>

Le véritable "couteau suisse" de la gestion de photo, permettant toutes sortes de classements et de recherches. Quelques fonctions de retouches légères toujours pratiques :

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/DigiKam.png">
    </div>
    <div>
      <h2>DigiKam</h2>
      <p>Gestionnaire de collection de photos</p>
      <div>
        <a href="https://framalibre.org/notices/digikam.html">Vers la notice Framalibre</a>
        <a href="https://www.digikam.org/">Vers le site</a>
      </div>
    </div>
  </article>

Plus léger mais rapide et efficace :

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Shotwell.png">
    </div>
    <div>
      <h2>Shotwell</h2>
      <p>Un logiciel de gestion de photothèque avancé, avec des options de retouche.</p>
      <div>
        <a href="https://framalibre.org/notices/shotwell.html">Vers la notice Framalibre</a>
        <a href="https://wiki.gnome.org/Apps/Shotwell">Vers le site</a>
      </div>
    </div>
  </article>

La référence du traitement RAW, très riche et performant :

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/darktable.png">
    </div>
    <div>
      <h2>darktable</h2>
      <p>Un puissant logiciel de traitement photo spécialisé dans le format RAW.</p>
      <div>
        <a href="https://framalibre.org/notices/darktable.html">Vers la notice Framalibre</a>
        <a href="http://www.darktable.org/">Vers le site</a>
      </div>
    </div>
  </article>

Incontournable pour de la retouche avancée, GIMP traverse les décennies et s'améliore régulièrement :

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/GIMP.png">
    </div>
    <div>
      <h2>GIMP</h2>
      <p>Un puissant logiciel d'édition et de retouche d'images.</p>
      <div>
        <a href="https://framalibre.org/notices/gimp.html">Vers la notice Framalibre</a>
        <a href="https://www.gimp.org/">Vers le site</a>
      </div>
    </div>
  </article>

Si vous n'aimez pas GIMP, vous pouvez utiliser Krita qui a l'avantage de pouvoir travailler simultanément en bitmap et vectoriel :

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Krita.png">
    </div>
    <div>
      <h2>Krita</h2>
      <p>Logiciel de création de dessins, peintures, logos et animations</p>
      <div>
        <a href="https://framalibre.org/notices/krita.html">Vers la notice Framalibre</a>
        <a href="https://krita.org/">Vers le site</a>
      </div>
    </div>
  </article>

Une sauvegarde sur le cloud est pratique et permet de partager ses photos sans enrichir les GAFAM :

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Nextcloud.png">
    </div>
    <div>
      <h2>Nextcloud</h2>
      <p>Nextcloud est une plate-forme auto-hébergée de services de stockage et d’applications diverses dans les nuages.</p>
      <div>
        <a href="https://framalibre.org/notices/nextcloud.html">Vers la notice Framalibre</a>
        <a href="https://nextcloud.com/">Vers le site</a>
      </div>
    </div>
  </article> 